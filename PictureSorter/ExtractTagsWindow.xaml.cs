﻿using System.Windows;

namespace PictureSorter
{
    /// <summary>
    /// Interaction logic for ExtractTagsWindow.xaml
    /// </summary>
    public partial class ExtractTagsWindow : Window
    {
        private bool avoidClosing = true;

        public ExtractTagsWindow()
        {
            InitializeComponent();
        }

        public void forceClose()
        {
            avoidClosing = false;
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = avoidClosing;
            this.Hide();
        }
    }

}