﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;

namespace PictureSorter
{
    class CTimeStampOffset
    {
        public bool Valid = false;
        public int Years = 0;
        public int Months = 0;
        public int Days = 0;
        public int Hours = 0;
        public int Minutes = 0;
        public int Seconds = 0;

        public void Parse(string years, string months, string days, string hours, string minutes, string seconds)
        {
            Valid = true;
            Valid = Valid && int.TryParse(years, out Years);
            Valid = Valid && int.TryParse(months, out Months);
            Valid = Valid && int.TryParse(days, out Days);
            Valid = Valid && int.TryParse(hours, out Hours);
            Valid = Valid && int.TryParse(minutes, out Minutes);
            Valid = Valid && int.TryParse(seconds, out Seconds);
        }

        public void AddToDateTime(ref DateTime dt)
        {
            if (Valid)
            {
                dt = dt.AddYears(Years);
                dt = dt.AddMonths(Months);
                dt = dt.AddDays(Days);
                dt = dt.AddHours(Hours);
                dt = dt.AddMinutes(Minutes);
                dt = dt.AddSeconds(Seconds);
            }
        }
    }

    class PictureProcessor
    {
        public enum EFileStatus
        {
            Unchanged, Duplicate, Sorted, NumStates
        }

        public static readonly List<string> PictureExtensions = new List<string> {
            ".jpg", ".jpeg", ".jpe", ".bmp", ".gif", ".png" };

        public string SrcPath { get; private set; } = "";
        public string DestPath { get; private set; } = "";
        public bool KeepSourceFiles { get; set; } = true;
        public bool ProcessOnlyPictures { get; set; } = false;
        public bool ExtractTagsFromName { get; set; } = false;
        public string OutputFormat { get; set; } = "";
        public string ExtractTagsTemplate { get; set; } = "";
        public CTimeStampOffset TimeStampOffset { get; set; } = new CTimeStampOffset();

        public bool setSrcPath(string path)
        {
            if(Directory.Exists(path))
            {
                SrcPath = path;
                return true;
            }
            return false;
        }

        public bool setDestPath(string path)
        {
            if(Directory.Exists(path))
            {
                DestPath = path;
                return true;
            }
            return false;
        }

        public static bool isPicture(string filePath)
        {
            return PictureExtensions.Contains(Path.GetExtension(filePath).ToLowerInvariant());
        }

        public Tuple<uint, uint> countFilesInDir(string inputDir)
        {
            var fileEntries = Directory.EnumerateFiles(inputDir, "*.*", SearchOption.AllDirectories);
            uint fileCount = (uint) fileEntries.Count();
            uint pictureCount = 0u;
            foreach (string fileName in fileEntries)
            {
                if (isPicture(fileName))
                {
                    ++pictureCount;
                }
            }
            return Tuple.Create(fileCount, pictureCount);
        }

        public void deleteEmptyDirs(string dirPath)
        {
            if(Directory.Exists(dirPath))
            {
                // recursively clear subdirectories
                foreach (string subdir in Directory.GetDirectories(dirPath))
                {
                    deleteEmptyDirs(subdir);
                }

                // delete directory if empty (do this after clearing subdirectories first)
                if (!Directory.EnumerateFileSystemEntries(dirPath).Any())
                {
                    Directory.Delete(dirPath);
                }
            }
        }

        public bool checkFilesEqual(string filePath1, string filePath2)
        {
            FileInfo file1 = new FileInfo(filePath1);
            FileInfo file2 = new FileInfo(filePath2);
            if (file1.Length == file2.Length)
            {
                var file1bytes = File.ReadAllBytes(filePath1);
                var file2bytes = File.ReadAllBytes(filePath2);
                return file1bytes.SequenceEqual(file2bytes);
            }
            return false;
        }

        public uint[] processDirectory(string inputDir)
        {
            var results = new uint[(int)EFileStatus.NumStates];

            if (inputDir.CompareTo(DestPath) == 0)
            {
                return results;
            }

            // Process the list of files found in the directory.
            results = processFilesInDir(inputDir);

            // Recurse into subdirectories of this directory.
            var subdirectoryEntries = Directory.GetDirectories(inputDir);
            foreach (string subdir in subdirectoryEntries)
            {
                var subResults  = processDirectory(subdir);
                for (int i = 0; i < results.Length; i++)
                {
                    results[i] += subResults[i];
                }
            }

            return results;
        }

        public uint[] processFilesInDir(string inputDir)
        {
            var results = new uint[(int)EFileStatus.NumStates];

            if (inputDir.CompareTo(DestPath) == 0)
            {
                return results;
            }

            // process files found in the directory
            string[] fileEntries = Directory.GetFiles(inputDir);
            foreach (string fileName in fileEntries)
            {
                if (!ProcessOnlyPictures || isPicture(fileName))
                {
                    var fileStatus = processFile(fileName);
                    ++results[(int)fileStatus];
                }
                // track only "relevant" files -> do not increment "Unchanged" in the else branch
            }
            return results;
        }

        public EFileStatus processFile(string filePath)
        {
            Console.WriteLine(filePath);

            EFileStatus fileStatus = EFileStatus.Unchanged;

            DateTime dt = ExtractTagsFromName ? 
                extractDateTimeFromName(filePath, ExtractTagsTemplate) : extractDateTimeFromMetadata(filePath);

            TimeStampOffset.AddToDateTime(ref dt);

            // If source files are kept, always sort
            // otherwise only sort if sorting is possible
            string newFilePath = "";
            if (KeepSourceFiles || !dt.Equals(DateTime.MinValue))
            {
                newFilePath = determineNewFilePath(filePath, dt);
                if (newFilePath.Length == 0)
                {
                    fileStatus = EFileStatus.Duplicate;
                }
            }

            if(newFilePath.Length > 0)
            {
                try
                {
                    if (KeepSourceFiles)
                    {
                        File.Copy(filePath, newFilePath);
                    }
                    else
                    {
                        File.Move(filePath, newFilePath);
                    }
                    fileStatus = EFileStatus.Sorted; // is assigned only after transfer was successful

                    // if tags were extracted from name, adapt meta data accordingly
                    // 2do: does not work. exception: Specified value of type 'System.Windows.Media.Imaging.BitmapMetadata' must have IsFrozen set to false to modify
                    //if (ExtractTagsFromName
                    //    && isPicture(newFilePath))
                    //{
                    //    BitmapSource img = BitmapFrame.Create(new Uri(newFilePath, UriKind.Relative), 
                    //                                          BitmapCreateOptions.None, 
                    //                                          BitmapCacheOption.OnLoad);
                    //    BitmapMetadata metaData = (BitmapMetadata)img.Metadata;
                    //    metaData.DateTaken = dt.ToString();
                    //}
                }
                catch (Exception e)
                {
                    Console.WriteLine("Caught exception: " + e.Message);
                }
            }

            return fileStatus;
        }

        // stitch new file path from date time
        private string determineNewFilePath(string filePath, DateTime dt)
        {
            string fileExt = Path.GetExtension(filePath).ToLowerInvariant();
            string newFileDir = DestPath;
            string newFileName = Path.GetFileName(filePath);

            if (!dt.Equals(DateTime.MinValue))
            {
                if (!string.IsNullOrEmpty(OutputFormat))
                {
                    string outputName = createNameFromDateTime(dt, OutputFormat);
                    
                    if (outputName.EndsWith(Path.DirectorySeparatorChar.ToString()) || outputName.EndsWith(Path.AltDirectorySeparatorChar.ToString()))
                    {
                        // outputName is only a directory -> keep original filename
                        newFileName = outputName + newFileName;
                    }
                    else
                    {
                        newFileName = outputName + fileExt;
                    }
                }
            }
            else
            {
                newFileDir = Path.Combine(newFileDir, "unsorted");
            }

            string newFilePath = Path.Combine(newFileDir, newFileName);
            Directory.CreateDirectory(Path.GetDirectoryName(newFilePath));

            // if already exists, check for duplicates and otherwise find new name
            if (File.Exists(newFilePath))
            {
                if (checkFilesEqual(newFilePath, filePath))
                {
                    newFilePath = "";
                }
                else
                {
                    // if filename is already present, add index (except for duplicates)
                    int fileIdx = 1;
                    string tmpPath;
                    do
                    {
                        ++fileIdx;
                        tmpPath = Path.ChangeExtension(newFilePath, null) + "_" + fileIdx.ToString() + fileExt;
                    } while (File.Exists(tmpPath));
                    newFilePath = tmpPath;
                }
            }

            return newFilePath;
        }

        private static DateTime extractDateTimeFromMetadata(string filePath)
        {
            DateTime dt = DateTime.MinValue;
            if (isPicture(filePath))
            {
                // pictures
                BitmapSource img = BitmapFrame.Create(new Uri(filePath, UriKind.Relative),
                                                      BitmapCreateOptions.None,
                                                      BitmapCacheOption.OnLoad);
                BitmapMetadata meta = (BitmapMetadata)img.Metadata;
                dt = Convert.ToDateTime(meta.DateTaken);
            }

            if (dt.Equals(DateTime.MinValue))
            {
                FileInfo finfo = new FileInfo(filePath);
                // CreationTime is modified when copying/moving file -> use LastWriteTime
                dt = finfo.LastWriteTime;
            }
            return dt;
        }

        private static DateTime extractDateTimeFromName(string filePath, string nameTemplate)
        {
            DateTime dt = DateTime.MinValue;

            string fileName = Path.GetFileNameWithoutExtension(filePath);

            // the rightmost bits are set for each correct date
            // i.e. all 6 are set: checkbyte = 00111111
            // which is 0x3F
            byte checkByte = 0;

            while (fileName.Length > 0)
            {
                // find begin of tag
                int tagBegin = nameTemplate.IndexOf('%');
                if(tagBegin < 0 || tagBegin > fileName.Length)
                {
                    break;
                }

                string substr = nameTemplate.Substring(0, tagBegin);
                if(!substr.Equals(fileName.Substring(0, tagBegin)))
                {
                    break;
                }

                fileName = fileName.Remove(0, tagBegin);
                nameTemplate = nameTemplate.Remove(0, tagBegin + 1); // remove % from refString as well

                // find end of tag
                int tagEnd = nameTemplate.IndexOf('%');
                if (tagEnd < 0 || tagEnd > fileName.Length)
                {
                    break;
                }

                // extract tag
                string tag = nameTemplate.Substring(0, tagEnd);
                char tagSign = tag[0];
                foreach( char c in tag)
                {
                    if (c != tagSign)
                    {
                        tagSign = '\0';
                        break;
                    }
                }

                string tagValueStr = fileName.Substring(0, tag.Length);
                int tagValue = 0;
                Int32.TryParse(tagValueStr, out tagValue);

                switch (tagSign)
                {
                    // subtract 1 for year/month/day as default DateTime is 01.01.0001
                    case 'y': dt = dt.AddYears(tagValue - 1);  checkByte |= 1 << 0; break;
                    case 'm': dt = dt.AddMonths(tagValue - 1); checkByte |= 1 << 1; break;
                    case 'd': dt = dt.AddDays(tagValue - 1);   checkByte |= 1 << 2; break;
                    case 'H': dt = dt.AddHours(tagValue);      checkByte |= 1 << 3; break;
                    case 'M': dt = dt.AddMinutes(tagValue);    checkByte |= 1 << 4; break;
                    case 'S': dt = dt.AddSeconds(tagValue);    checkByte |= 1 << 5; break;
                }

                fileName = fileName.Remove(0, tag.Length);
                nameTemplate = nameTemplate.Remove(0, tagEnd + 1); // remove % from refString as well
            }

            Console.WriteLine(fileName + " -> " + dt);

            if (checkByte != 0x3F)
            {
                dt = DateTime.MinValue;
                Console.WriteLine("Could not parse DateTime");
            }

            return dt;
        }

        private static string createNameFromDateTime(DateTime dt, string nameTemplate)
        {
            string result = nameTemplate;

            while (true)
            {
                int tagBegin = result.IndexOf('%');
                if (tagBegin < 0 || tagBegin > result.Length)
                {
                    break;
                }
                int tagEnd = result.IndexOf('%', tagBegin + 1);
                if (tagEnd < 0 || tagEnd > result.Length)
                {
                    break;
                }

                string tag = result.Substring(tagBegin + 1, tagEnd - tagBegin - 1);

                // switch tag characters to replicate Python datetime formatting (lowercase=date, uppercase=time)
                tag = tag.Replace("S", "s");  // switch Seconds
                tag = tag.Replace("M", "%").Replace("m", "M").Replace("%", "m");  // switch month and Minute

                string replacedTag = dt.ToString(tag, CultureInfo.InvariantCulture);
                result = result.Substring(0, tagBegin) + replacedTag + result.Substring(tagEnd + 1);
            }

            return result;
        }
    }
}
