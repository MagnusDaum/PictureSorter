﻿using System;
using System.Windows;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;

namespace PictureSorter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PictureProcessor m_picProcessor = new PictureProcessor();
        private BackgroundWorker m_worker;
        private ExtractTagsWindow m_extractTagsWindow = new ExtractTagsWindow();
        private TimeStampOffsetWindow m_timestampOffsetWindow = new TimeStampOffsetWindow();
        private DateTime m_startTime;

        public MainWindow()
        {
            InitializeComponent();

            m_worker = new BackgroundWorker();
            m_worker.WorkerReportsProgress = true;
            m_worker.WorkerSupportsCancellation = true;

            m_worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            m_worker.ProgressChanged +=
                        new ProgressChangedEventHandler(worker_ProgressChanged);
            m_worker.RunWorkerCompleted +=
                       new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }

        public static uint arraySum(uint[] array)
        {
            uint sum = 0;
            foreach (uint i in array)
            {
                sum += i;
            }
            return sum;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            m_extractTagsWindow.forceClose();
            m_timestampOffsetWindow.forceClose();
            base.OnClosing(e);
        }

        private void mainform_Initialized(object sender, EventArgs e)
        {
            src_path_textbox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            dest_path_textbox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + Path.DirectorySeparatorChar + ResStrings.Title;

            keepSourceFiles_checkItem.IsChecked  = m_picProcessor.KeepSourceFiles;
            sortPicturesOnly_checkItem.IsChecked = m_picProcessor.ProcessOnlyPictures;
            tagsFromFileName_checkItem.IsChecked = m_picProcessor.ExtractTagsFromName;
        }

        private string ShowPathDialog(string startPath)
        {
            var folder_dialog = new System.Windows.Forms.FolderBrowserDialog();
            folder_dialog.SelectedPath = startPath;
            var dialogRes = folder_dialog.ShowDialog();
            if (dialogRes == System.Windows.Forms.DialogResult.OK)
            {
                return folder_dialog.SelectedPath;
            }
            return "";
        }

        private bool GetNullableBoolValue(bool? b, bool defaultValue)
        {
            return b.HasValue ? (bool)b : defaultValue;
        }

        ///////////////////////////////////////////////////////////////////////
        // Event Handler
        ///////////////////////////////////////////////////////////////////////

        private void src_path_button_Click(object sender, RoutedEventArgs e)
        {
            string path = ShowPathDialog(src_path_textbox.Text);
            if (path.Length > 0)
            {
                src_path_textbox.Text = path;
                dest_path_textbox.Text = path + "_sorted";
            }
        }

        private void dest_path_button_Click(object sender, RoutedEventArgs e)
        {
            string path = ShowPathDialog(dest_path_textbox.Text);
            if (path.Length > 0)
            {
                dest_path_textbox.Text = path;
            }
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void scan_Click(object sender, RoutedEventArgs e)
        {
            m_startTime = DateTime.Now;
            statusbar_textblock.Text = "";
            if (!m_picProcessor.setSrcPath(src_path_textbox.Text))
            {
                return;
            }

            log_textblock.Text += $"{ResStrings.Scanning} {src_path_textbox.Text}...\n";
            Tuple<uint, uint> dirCount = m_picProcessor.countFilesInDir(m_picProcessor.SrcPath);
            log_textblock.Text += ResStrings.Files + ": " + dirCount.Item1.ToString() + "\n" + ResStrings.Pictures + ": " + dirCount.Item2.ToString() + "\n";
            log_textblock.Text += "------------\n";
        }

        private void sort_Click(object sender, RoutedEventArgs e)
        {
            m_startTime = DateTime.Now;
            statusbar_textblock.Text = "";
            if ( !m_picProcessor.setSrcPath(src_path_textbox.Text) )
            {
                return;
            }
            Directory.CreateDirectory(dest_path_textbox.Text);
            if (!m_picProcessor.setDestPath(dest_path_textbox.Text))
            {
                return;
            }

            m_picProcessor.KeepSourceFiles = GetNullableBoolValue(keepSourceFiles_checkItem.IsChecked, true);
            m_picProcessor.ProcessOnlyPictures = GetNullableBoolValue(sortPicturesOnly_checkItem.IsChecked, false);
            m_picProcessor.ExtractTagsFromName = GetNullableBoolValue(tagsFromFileName_checkItem.IsChecked, false);
            m_picProcessor.OutputFormat = output_format_textbox.Text;
            m_picProcessor.ExtractTagsTemplate = m_extractTagsWindow.tagFormatTextbox.Text;
            m_picProcessor.TimeStampOffset.Parse(
                m_timestampOffsetWindow.years_textbox.Text,
                m_timestampOffsetWindow.months_textbox.Text,
                m_timestampOffsetWindow.days_textbox.Text,
                m_timestampOffsetWindow.hours_textbox.Text,
                m_timestampOffsetWindow.minutes_textbox.Text,
                m_timestampOffsetWindow.seconds_textbox.Text);

            m_worker.RunWorkerAsync(0);
        }

        private void tagsFromFileName_checkItem_Click(object sender, RoutedEventArgs e)
        {
            if (GetNullableBoolValue(tagsFromFileName_checkItem.IsChecked, false))
            {
                m_extractTagsWindow.Show();
            }
        }

        private void timestamp_offset_Click(object sender, RoutedEventArgs e)
        {
            m_timestampOffsetWindow.Show();
        }

        ///////////////////////////////////////////////////////////////////////
        // Background worker
        ///////////////////////////////////////////////////////////////////////

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            m_worker.ReportProgress(-1, $"{ResStrings.Scanning}...\n");

            Tuple<uint, uint> dirCount = m_picProcessor.countFilesInDir(m_picProcessor.SrcPath);
            uint targetSortedCount = m_picProcessor.ProcessOnlyPictures ? dirCount.Item2 : dirCount.Item1;
            m_worker.ReportProgress(-1, ResStrings.Files +": " + dirCount.Item1.ToString() + "\n" + ResStrings.Pictures + ": " + dirCount.Item2.ToString() + "\n");

            m_worker.ReportProgress(-1, ResStrings.Sort + "...\n");

            m_worker.ReportProgress(-1, $".{Path.DirectorySeparatorChar} ... ");
            var fileStatistics = m_picProcessor.processFilesInDir(m_picProcessor.SrcPath);
            m_worker.ReportProgress((int) (arraySum(fileStatistics) * 100 / targetSortedCount));

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(m_picProcessor.SrcPath);
            foreach (string subdirectory in subdirectoryEntries)
            {
                m_worker.ReportProgress(-1, $".{Path.DirectorySeparatorChar}{System.IO.Path.GetFileName(subdirectory)} ... ");
                var subResults  = m_picProcessor.processDirectory(subdirectory);
                for (int i = 0; i < fileStatistics.Length; i++)
                {
                    fileStatistics[i] += subResults[i];
                }
                m_worker.ReportProgress((int)(arraySum(fileStatistics) * 100 / targetSortedCount));
            }

            uint numSorted = fileStatistics[(int)PictureProcessor.EFileStatus.Sorted];
            uint numDuplicate = fileStatistics[(int)PictureProcessor.EFileStatus.Duplicate];
            m_worker.ReportProgress(-1, ResStrings.Sorted + ": " + numSorted.ToString() + "\n");
            m_worker.ReportProgress(-1, ResStrings.Duplicates + ": " + numDuplicate.ToString() + "\n");

            m_picProcessor.deleteEmptyDirs(m_picProcessor.SrcPath);
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage >= 0)
            {
                log_textblock.Text += e.ProgressPercentage.ToString() + "%\n";
                statusbar_textblock.Text = e.ProgressPercentage.ToString() + "%";
            }
            else
            {
                log_textblock.Text += e.UserState as String;
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            log_textblock.Text += $"took {new DateTime((DateTime.Now - m_startTime).Ticks):HH:mm:ss}\n";
            log_textblock.Text += "------------------------------------\n";
            this.Activate();
        }
    }
}

